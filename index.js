
$( document ).ready(function() {

$(function($) {
  let url = window.location.href;
  $('ul li a').each(function() {
    if (this.href === url) {
      $(this).closest('li').addClass('active');
    }
  });
});

$('input[type="checkbox"]').on('change', function(){
  var data = $('input[type="checkbox"]').serialize(),
      loc = $('<a>', {href:window.location})[0];
  if(history.pushState){
      history.pushState(null, null, loc.pathname+'?'+data);
  }
});
})
